# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :admin_web,
  namespace: AdminWeb,
  ecto_repos: []

# Configures the endpoint
config :admin_web, AdminWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "aKLITtwpSkCY5zMW3MXAoa8oC/xGWoJOFrtg9o7g5xbo/do5AcSBkpic1rypQ03B",
  render_errors: [view: AdminWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: AdminWeb.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :admin_web, :generators,
  context_app: false

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
