defmodule Blog.Hubs.Hub do
  use Ecto.Schema
  import Ecto.Changeset
  alias Blog.Hubs.Hub


  schema "hubs" do
    belongs_to :created_by, Blog.Account.User
    belongs_to :updated_by, Blog.Account.User

    field :adult, :boolean, default: false
    field :description, :string
    field :name, :string
    field :url, :string
    field :posts_count, :integer

    field :created_from_ip, EctoNetwork.INET
    field :created_from_country_code, :string
    field :updated_from_ip, EctoNetwork.INET
    field :updated_from_country_code, :string

    has_many :posts, Blog.Posts.Post

    timestamps()
  end

  @doc false
  def changeset(%Hub{} = hub, attrs) do
    hub
    |> cast(attrs, [
      :created_by_id,
      :name,
      :description,
      :adult,
      :url,
      :created_from_ip,
      :created_from_country_code
    ])
    |> validate_required([:name, :description, :adult, :created_by_id, :url])
    |> foreign_key_constraint(:created_by_id)
    |> unique_constraint(:url)
    |> validate_format(:url, ~r/^[\w\-]+$/)
    |> validate_length(:url, min: 1, max: 15)
  end

  def update_changeset(%Hub{} = hub, attrs) do
    hub
    |> cast(attrs, [
      :name,
      :description,
      :adult,
      :updated_by_id,
      :updated_from_ip,
      :updated_from_country_code
    ])
    |> validate_required([
      :name,
      :description,
      :adult,
      :updated_by_id
    ])
    |> foreign_key_constraint(:updated_by_id)
  end
end
