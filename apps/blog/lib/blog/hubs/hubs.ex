defmodule Blog.Hubs do
  @moduledoc """
  The Hubs context.
  """

  import Ecto.Query, warn: false
  alias Blog.Repo

  alias Blog.Hubs.Hub

  @doc """
  Returns the list of hubs.

  ## Examples

      iex> list_hubs()
      [%Hub{}, ...]

  """
  def list_hubs do
    Repo.all(Hub) |> Repo.preload(:created_by)
  end

  def list_hubs(name, page \\ 1, limit \\ 20) do
    from(hub in Hub, where: (ilike(hub.name, ^"#{name}%") or ilike(hub.url, ^"#{name}%")))
    |> from(limit: ^limit)
    |> from(offset: ^((page - 1) * limit))
    |> Repo.all
  end

  def get_hub(nil), do: nil
  def get_hub(id), do: Repo.get(Hub, id)

  @doc """
  Gets a single hub.

  Raises `Ecto.NoResultsError` if the Hub does not exist.

  ## Examples

      iex> get_hub!(123)
      %Hub{}

      iex> get_hub!(456)
      ** (Ecto.NoResultsError)

  """
  def get_hub!(id) when is_integer(id), do: Repo.get!(Hub, id) |> Repo.preload(:created_by)
  def get_hub!(url) when is_binary(url), do: Repo.get_by!(Hub, %{url: url}) |> Repo.preload(:created_by)

  @doc """
  Creates a hub.

  ## Examples

      iex> create_hub(%{field: value})
      {:ok, %Hub{}}

      iex> create_hub(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_hub(attrs \\ %{}) do
    %Hub{}
    |> Hub.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a hub.

  ## Examples

      iex> update_hub(hub, %{field: new_value})
      {:ok, %Hub{}}

      iex> update_hub(hub, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_hub(%Hub{} = hub, attrs) do
    hub
    |> Hub.update_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Hub.

  ## Examples

      iex> delete_hub(hub)
      {:ok, %Hub{}}

      iex> delete_hub(hub)
      {:error, %Ecto.Changeset{}}

  """
  def delete_hub(%Hub{} = hub) do
    Repo.delete(hub)
  end

  def update_posts_counter!(hub_id) do
    Hub
    |> where([p], p.id == ^hub_id)
    |> Repo.update_all(set: [posts_count: Blog.Posts.count_by_hub_id(hub_id)])
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking hub changes.

  ## Examples

      iex> change_hub(hub)
      %Ecto.Changeset{source: %Hub{}}

  """
  def change_hub(%Hub{} = hub) do
    Hub.changeset(hub, %{})
  end
end
