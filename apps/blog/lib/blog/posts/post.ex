defmodule Blog.Posts.Post do
  use Ecto.Schema
  import Ecto.Changeset
  alias Blog.Posts.Post


  schema "posts" do
    field :body, :string
    field :cut, :string
    field :slug, :string
    field :title, :string
    field :url, :string
    belongs_to :hub, Blog.Hubs.Hub
    belongs_to :created_by, Blog.Account.User
    belongs_to :updated_by, Blog.Account.User

    timestamps()
  end

  @doc false
  def changeset(%Post{} = post, attrs) do
    post
    |> cast(attrs, [:title, :hub_id, :url, :cut, :body, :created_by_id, :updated_by_id])
    |> validate_required([:title, :cut, :created_by_id, :updated_by_id])
    |> foreign_key_constraint(:created_by_id)
    |> foreign_key_constraint(:updated_by_id)
    |> foreign_key_constraint(:hub_id)
    |> validate_format(:url, ~r/^\w+:(\/?\/?)[^\s]+$/)
    |> slugify
    |> unique_constraint(:slug)
  end

  defp slugify(changeset) do
    changeset
    |> put_change(:slug, Slug.slugify(get_field(changeset, :title) || ""))
  end
end
