defmodule Blog.Posts do
  @moduledoc """
  The Posts context.
  """

  import Ecto.Query, warn: false
  alias Blog.Repo

  alias Blog.Posts.Post

  @doc """
  Returns the list of posts.

  ## Examples

      iex> list_posts()
      [%Post{}, ...]

  """
  def list_posts do
    Post
    |> where([p], not is_nil(p.hub_id) )
    |> order_by([p], desc: :inserted_at)
    |> Repo.all
  end

  def list_posts_by_hub(%Blog.Hubs.Hub{} = hub) do
    Post
    |> where([p], p.hub_id == ^hub.id)
    |> order_by([p], desc: :inserted_at)
    |> Repo.all
    |> Repo.preload([:hub, :created_by])
  end

  @doc """
  Gets a single post.

  Raises `Ecto.NoResultsError` if the Post does not exist.

  ## Examples

      iex> get_post!(123)
      %Post{}

      iex> get_post!(456)
      ** (Ecto.NoResultsError)

  """
  def get_post!(id) when is_integer(id), do: Repo.get!(Post, id)

  def get_post!(slug) when is_binary(slug), do: Repo.get_by!(Post, %{slug: slug})

  @doc """
  Creates a post.

  ## Examples

      iex> create_post(%{field: value})
      {:ok, %Post{}}

      iex> create_post(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_post(attrs \\ %{}) do
    %Post{}
    |> Post.changeset(attrs)
    |> Repo.insert(returning: [:id, :slug])
    |> hub_post_update_counter
  end

  @doc """
  Updates a post.

  ## Examples

      iex> update_post(post, %{field: new_value})
      {:ok, %Post{}}

      iex> update_post(post, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_post(%Post{} = post, attrs) do
    updated_data = post
                   |> Post.changeset(attrs)
                   |> Repo.update()
                   |> hub_post_update_counter(post)

    case updated_data do
      {:ok, post} -> {:ok, get_post!(post.id)}
      _ -> updated_data
    end
  end

  @doc """
  Deletes a Post.

  ## Examples

      iex> delete_post(post)
      {:ok, %Post{}}

      iex> delete_post(post)
      {:error, %Ecto.Changeset{}}

  """
  def delete_post(%Post{} = post) do
    post
    |> Repo.delete
    |> hub_post_update_counter(:delete)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking post changes.

  ## Examples

      iex> change_post(post)
      %Ecto.Changeset{source: %Post{}}

  """
  def change_post(%Post{} = post) do
    Post.changeset(post, %{})
  end

  def count_by_hub_id(hub_id) do
    Post
    |> where([p], p.hub_id == ^hub_id)
    |> select(count("*"))
    |> Repo.one
  end

  defp hub_post_update_counter(data) do
    case data do
      {:ok, post} -> if post.hub_id, do: Blog.Hubs.update_posts_counter!(post.hub_id)
      _ -> data
    end
    data
  end

  defp hub_post_update_counter(data, %Post{} = old_post) do
    case data do
      {:ok, new_post} ->
        if new_post.hub_id, do: Blog.Hubs.update_posts_counter!(new_post.hub_id)
        if old_post.hub_id, do: Blog.Hubs.update_posts_counter!(old_post.hub_id)
      _ -> data
    end
    data
  end

  defp hub_post_update_counter(data, :delete) do
    case data do
      {:ok, post} ->
        if post.hub_id, do: Blog.Hubs.update_posts_counter!(post.hub_id)
      _-> data
    end
    data
  end


  def editable?(%Blog.Posts.Post{} = post, nil), do: false

  def editable?(%Blog.Posts.Post{} = post, %Blog.Account.User{} = current_user) do
    post.created_by_id == current_user.id
  end

  def deletable?(%Blog.Posts.Post{} = post, nil), do: false

  def deletable?(%Blog.Posts.Post{} = post, %Blog.Account.User{} = current_user) do
    post.created_by_id == current_user.id
  end
end
