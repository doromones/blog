defmodule Blog.Blog.GeoIP do
  use Ecto.Schema
  import Ecto.Changeset
  alias Blog.Blog.GeoIP


  schema "geoip" do
    field :country_code, :string
    field :event, :string
    field :ipv4, EctoNetwork.INET
    field :ipv6, EctoNetwork.INET
    belongs_to :user, Blog.Account.User

    timestamps()
  end

  @doc false
  def changeset(%GeoIP{} = geo_ip, attrs) do
    geo_ip
    |> cast(attrs, [:event, :ipv4, :ipv6, :country_code])
    |> validate_required([:event, :ipv4, :ipv6, :country_code])
  end
end
