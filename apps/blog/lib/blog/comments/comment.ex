defmodule Blog.Comments.Comment do
  use Ecto.Schema
  import Ecto.Changeset
  alias Blog.Comments.Comment

  schema "comments" do
    belongs_to :created_by, Blog.Account.User
    belongs_to :updated_by, Blog.Account.User
    belongs_to :post, Blog.Hubs.Hub
    belongs_to :parent, __MODULE__

    field :body, :string
    field :created_from_country_code, :string
    field :created_from_ip, EctoNetwork.INET
    field :updated_from_country_code, :string
    field :updated_from_ip, EctoNetwork.INET

    timestamps()
  end

  @doc false
  def changeset_create(%Comment{} = comment, attrs) do
    comment
    |> cast(attrs, [:parent_id, :post_id, :created_by_id, :created_from_ip, :created_from_country_code, :body])
    |> validate_required([:created_by_id, :created_from_ip, :body])
    |> foreign_key_constraint(:post_id)
    |> foreign_key_constraint(:created_by_id)
    |> validate_length(:body, min: 1)
    |> set_left
  end

  def changeset_update(%Comment{} = comment, attrs) do
    comment
    |> cast(attrs, [:updated_by_id, :updated_from_ip, :updated_from_country_code, :body])
    |> validate_required([:updated_by_id, :updated_from_ip, :body])
    |> foreign_key_constraint(:updated_by_id)
    |> validate_length(:body, min: 1)
  end

  defp set_left(changeset) do
    if get_field(changeset, :parent_id) do
      changeset
      |> put_change(:left, Comments.get_comment!(get_field(changeset, :parent_id)).left + 1)
    else
      changeset
    end
  end
end
