defmodule Blog.Account do
  @moduledoc """
  The Account context.
  """

  import Ecto.{Query, Changeset}, warn: false
  import Comeonin.Bcrypt, only: [hashpwsalt: 1]

  alias Blog.Repo
  alias Blog.Account.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  def get_user_by_username(username), do: Repo.get_by(User, username: username)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> user_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> user_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  def check_auth(username, password) do
    user = get_user_by_username(username)
    if user && Comeonin.Bcrypt.checkpw(password, user.password_digest) do
      {:ok, user}
    else
      {:error}
    end
  end

  def update_user_after_auth(%User{} = user, attrs) do
    user
    |> user_login_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    user_changeset(user, %{})
  end

  @doc false
  def user_login_changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [
      :last_login_from_ip,
      :last_login_from_country_code,
      :last_login_at
    ])
  end

  @doc false
  def user_changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [
      :username,
      :email,
      :password,
      :password_confirmation,
      :registered_from_ip,
      :registered_from_country_code,
    ])
    |> validate_required([:username, :email, :password, :password_confirmation])
    |> validate_confirmation(:password)
    |> validate_format(:username, ~r/^[\w]+$/)
    |> update_change(:email, &String.downcase/1)
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
    |> unique_constraint(:username)
    |> hash_password
    |> put_change(:password, nil)
    |> put_change(:password_confirmation, nil)
  end

  @doc false
  defp hash_password(changeset) do
    if password = get_change(changeset, :password) do
      changeset
      |> put_change(:password_digest, hashpwsalt(password))
    else
      changeset
    end
  end
end
