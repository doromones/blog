defmodule Blog.Account.User do
  @moduledoc """
    User schema
  """

  use Ecto.Schema

  schema "users" do
    field :username, :string
    field :email, :string
    field :password_digest, :string
#    field :admin?, :boolean, source: :is_admin

    field :registered_from_ip, EctoNetwork.INET
    field :last_login_from_ip, EctoNetwork.INET
    field :registered_from_country_code, :string
    field :last_login_from_country_code, :string
    field :last_login_at, :naive_datetime

    timestamps()

    # Virtual Fields
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
  end
end
