defmodule Blog.Repo.Migrations.AddTriggerSetSlugToPosts do
  use Ecto.Migration

  def up do
    execute """
      CREATE FUNCTION posts_set_slug() RETURNS trigger AS $emp_stamp$
      BEGIN
        IF NEW.id = 0
        THEN
          NEW.slug := NEW.slug || '-' || currval('posts_id_seq');
        ELSE
          NEW.slug := NEW.slug || '-' || NEW.id;
        END IF;

        RETURN NEW;
      END;
      $emp_stamp$ LANGUAGE plpgsql;
    """

    execute """
      CREATE TRIGGER posts_set_slug BEFORE INSERT OR UPDATE ON posts
      FOR EACH ROW EXECUTE PROCEDURE posts_set_slug();
    """
  end

  def down do
    execute "DROP TRIGGER posts_set_slug ON posts"
    execute "DROP FUNCTION posts_set_slug();"
  end
end
