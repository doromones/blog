defmodule Blog.Repo.Migrations.CreateGeoip do
  use Ecto.Migration

  def change do
    create table(:geoip) do
      add :event, :string
      add :ip, :inet
      add :country_code, :string, size: 2
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:geoip, [:user_id])
    create index(:geoip, [:ip])
    create index(:geoip, [:country_code])
    create index(:geoip, [:event])

    alter table(:posts) do
      add :created_from_ip, :inet
      add :created_from_country_code, :string, size: 2
      add :updated_from_ip, :inet
      add :updated_from_country_code, :string, size: 2
    end

    alter table(:hubs) do
      add :updated_by_id, references(:users, on_delete: :nothing)
      add :created_from_ip, :inet
      add :created_from_country_code, :string, size: 2
      add :updated_from_ip, :inet
      add :updated_from_country_code, :string, size: 2
    end

    alter table(:users) do
      add :registered_from_ip, :inet
      add :registered_from_country_code, :string, size: 2
      add :last_login_from_ip, :inet
      add :last_login_from_country_code, :string, size: 2
      add :last_login_at, :utc_datetime
    end
  end
end
