defmodule Blog.Repo.Migrations.AddHubPostConterCache do
  use Ecto.Migration

  def change do
    alter table(:hubs) do
      add :posts_count, :integer, default: 0, null: false
    end
  end
end
