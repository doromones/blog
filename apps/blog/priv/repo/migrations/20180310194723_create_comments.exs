defmodule Blog.Repo.Migrations.CreateComments do
  use Ecto.Migration

  def change do
    create table(:comments) do
      add :post_id, references(:posts, on_delete: :nothing), null: false
      add :parent_id, references(:comments, on_delete: :nothing)
      add :left, :integer, default: 0
      add :created_by_id, references(:users, on_delete: :nothing), null: false
      add :updated_by_id, references(:users, on_delete: :nothing)
      add :created_from_ip, :inet, null: false
      add :created_from_country_code, :string, limit: 2
      add :updated_from_ip, :inet
      add :updated_from_country_code, :string, limit: 2
      add :body, :text, null: false

      timestamps()
    end

    create index(:comments, [:post_id])
    create index(:comments, [:created_by_id])
    create index(:comments, [:updated_by_id])
  end
end
