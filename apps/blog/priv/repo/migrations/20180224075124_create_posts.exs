defmodule Blog.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def change do
    create table(:posts) do
      add :title, :string, null: false
      add :cut, :text, null: false
      add :body, :text, default: nil
      add :slug, :string, null: false
      add :url, :text, default: nil
      add :hub_id, references(:hubs, on_delete: :nothing)
      add :created_by_id, references(:users, on_delete: :nothing)
      add :updated_by_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:posts, [:hub_id])
    create index(:posts, [:created_by_id])
    create index(:posts, [:updated_by_id])
    create index(:posts, [:slug], unique: true)
  end
end
