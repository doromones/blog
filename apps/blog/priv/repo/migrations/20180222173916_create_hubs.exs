defmodule Blog.Repo.Migrations.CreateHubs do
  use Ecto.Migration

  def change do
    create table(:hubs) do
      add :created_by_id, references(:users, on_delete: :nothing)
      add :url, :string, default: false, null: false
      add :name, :string, default: false, null: false
      add :description, :text, default: false, null: false
      add :adult, :boolean, default: false

      timestamps()
    end

    create index(:hubs, [:created_by_id])
    create index(:hubs, [:url], unique: true)
  end
end
