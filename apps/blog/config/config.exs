use Mix.Config

config :blog, ecto_repos: [Blog.Repo]

config :geolix,
       databases: [
         %{
           id: :city,
           adapter: Geolix.Adapter.MMDB2,
           source: [__DIR__, "../priv/GeoLite2-City.mmdb"]
                   |> Path.join
                   |> Path.expand
         },
         %{
           id: :country,
           adapter: Geolix.Adapter.MMDB2,
           source: [__DIR__, "../priv/GeoLite2-Country.mmdb"]
                   |> Path.join
                   |> Path.expand
         }
       ]

import_config "#{Mix.env}.exs"
