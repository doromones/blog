use Mix.Config

# Configure your database
config :blog, Blog.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "password",
  database: "blog_dev",
  hostname: "localhost",
  pool_size: 10

config :comeonin, bcrypt_log_rounds: 4
