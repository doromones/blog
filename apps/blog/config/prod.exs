use Mix.Config

import_config "prod.secret.exs"

config :comeonin, bcrypt_log_rounds: 12
