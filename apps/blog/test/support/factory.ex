defmodule Blog.Factory do
  # with Ecto
  use ExMachina.Ecto, repo: Blog.Repo
  import Comeonin.Bcrypt, only: [hashpwsalt: 1]

  alias Blog.Account.User
  alias Blog.Hubs.Hub
  alias Blog.Posts.Post

  def user_factory do
    %User{
      username: sequence(:username, &"username#{&1}"),
      email: sequence(:email, &"email-#{&1}@example.com"),
      password_digest: hashpwsalt("password"),
      password: nil,
      password_confirmation: nil
    }
  end

  def with_password(%{} = user) do
    user |> Map.merge(%{"password" => "password", "password_confirmation" => "password"})
  end

  def hub_factory do
    %Hub{
      name: "hub name",
      description: "hub description",
      adult: false,
      url: sequence(:url, &"url#{&1}"),
      created_by: build(:user),
      posts_count: 0
    }
  end

  def post_factory do
    user = insert(:user)
    %Post{
      title: sequence(:title, &"title-#{&1}"),
      cut: sequence(:cut, &"cut-#{&1}"),
      body: sequence(:body, &"body-#{&1}"),
      slug: sequence(:slug, &"slug-#{&1}"),
      url: sequence(:url, &"http://example.com/url-#{&1}"),
      hub: build(:hub),
      created_by: user,
      created_by_id: user.id,
      updated_by: user,
      updated_by_id: user.id,
    }
  end
end