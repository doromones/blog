defmodule Blog.AccountTest do
  use Blog.DataCase
  import Blog.Factory

  alias Blog.Account

  describe "users" do
    alias Blog.Account.User

    @invalid_attrs %{username: nil, email: nil, password: nil, password_confirmation: nil}

    test "list_users/0 returns all users" do
      user = insert(:user)
      assert Account.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = insert(:user)
      assert Account.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      user_params = string_params_for(:user) |> with_password
      assert {:ok, %User{} = user} = Account.create_user(user_params)
      assert user.email == Map.get(user_params, "email")
      assert user.username == Map.get(user_params, "username")
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = insert(:user)
      user_params = params_for(:user, %{password: "password", password_confirmation: "password"})
      assert {:ok, user} = Account.update_user(user, user_params)
      assert %User{} = user
      assert user.email == user_params.email
      assert user.username == user_params.username
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = insert(:user)
      assert {:error, %Ecto.Changeset{}} = Account.update_user(user, @invalid_attrs)
      assert user == Account.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = insert(:user)
      assert {:ok, %User{}} = Account.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Account.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = insert(:user)
      assert %Ecto.Changeset{} = Account.change_user(user)
    end

    test "password_digest value gets set to a hash" do
      user_params = string_params_for(:user) |> with_password
      user = insert(:user, Map.new(user_params, fn {k, v} -> {String.to_atom(k), v} end))
      assert Comeonin.Bcrypt.checkpw(Map.get(user_params, "password"), user.password_digest)
    end
  end
end
