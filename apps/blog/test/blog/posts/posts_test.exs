defmodule Blog.PostsTest do
  use Blog.DataCase
  import Blog.Factory
  alias Blog.Posts

  describe "posts" do
    alias Blog.Posts.Post

    @update_attrs %{body: "some updated body", cut: "some updated cut", slug: "some updated slug", title: "some updated title", url: "http://example.com"}
    @invalid_attrs %{body: nil, cut: nil, slug: nil, title: nil, url: nil}

    test "list_posts/0 returns all posts" do
      post = build(:post) |> Repo.insert!(returning: true)
      assert Posts.list_posts() |> Repo.preload([:created_by, :updated_by, hub: :created_by]) == [post]
    end

    test "get_post!/1 returns the post with given id" do
      post = build(:post) |> Repo.insert!(returning: true)
      r_post = Posts.get_post!(post.id) |> Repo.preload([:created_by, :updated_by, hub: :created_by])
      assert  r_post == post
    end

    test "create_post/1 with valid data creates a post" do
      params = string_params_for(:post)
      assert {:ok, %Post{} = post} = Posts.create_post(params)
      require IEx; IEx.pry
      assert post.body == Map.get(params, "body")
      assert post.cut == Map.get(params, "cut")
      assert post.slug == Slug.slugify("#{Map.get(params, "title")} #{post.id}")
      assert post.title == Map.get(params, "title")
      assert post.url == Map.get(params, "url")
    end

    test "create_post/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Posts.create_post(@invalid_attrs)
    end

    test "update_post/2 with valid data updates the post" do
      post = insert(:post)
      assert {:ok, post} = Posts.update_post(post, @update_attrs)
      assert %Post{} = post
      assert post.body == "some updated body"
      assert post.cut == "some updated cut"
      assert post.slug == Slug.slugify("#{@update_attrs.title} #{post.id}")
      assert post.title == "some updated title"
      assert post.url == "http://example.com"
    end

    test "update_post/2 with invalid data returns error changeset" do
      post = build(:post) |> Repo.insert!(returning: true)
      assert {:error, %Ecto.Changeset{}} = Posts.update_post(post, @invalid_attrs)
      assert post == Posts.get_post!(post.id) |> Repo.preload([:created_by, :updated_by, hub: :created_by])
    end

    test "delete_post/1 deletes the post" do
      post = insert(:post)
      assert {:ok, %Post{}} = Posts.delete_post(post)
      assert_raise Ecto.NoResultsError, fn -> Posts.get_post!(post.id) end
    end

    test "change_post/1 returns a post changeset" do
      post = insert(:post)
      assert %Ecto.Changeset{} = Posts.change_post(post)
    end
  end
end
