defmodule Blog.HubsTest do
  use Blog.DataCase
  import Blog.Factory

  alias Blog.Hubs

  describe "hubs" do
    alias Blog.Hubs.Hub

    @invalid_attrs %{adult: nil, description: nil, name: nil}

    test "list_hubs/0 returns all hubs" do
      hub = insert(:hub)
      assert Hubs.list_hubs() == [hub]
    end

    test "get_hub!/1 returns the hub with given id" do
      hub = insert(:hub)
      assert Hubs.get_hub!(hub.id) == hub
    end

    test "create_hub/1 with valid data creates a hub" do
      hub_params = string_params_with_assocs(:hub, %{adult: true})
      assert {:ok, %Hub{} = hub} = Hubs.create_hub(hub_params)
      assert hub.adult == true
      assert hub.description == Map.get(hub_params, "description")
      assert hub.name == Map.get(hub_params, "name")
    end

    test "create_hub/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Hubs.create_hub(@invalid_attrs)
    end

    test "update_hub/2 with valid data updates the hub" do
      hub = insert(:hub)
      hub_params = string_params_with_assocs(:hub)
      assert {:ok, hub} = Hubs.update_hub(hub, hub_params)
      assert %Hub{} = hub
      assert hub.adult == false
      assert hub.description == Map.get(hub_params, "description")
      assert hub.name == Map.get(hub_params, "name")
    end

    test "update_hub/2 with invalid data returns error changeset" do
      hub = insert(:hub)
      assert {:error, %Ecto.Changeset{}} = Hubs.update_hub(hub, @invalid_attrs)
      assert hub == Hubs.get_hub!(hub.id)
    end

    test "delete_hub/1 deletes the hub" do
      hub = insert(:hub)
      assert {:ok, %Hub{}} = Hubs.delete_hub(hub)
      assert_raise Ecto.NoResultsError, fn -> Hubs.get_hub!(hub.id) end
    end

    test "change_hub/1 returns a hub changeset" do
      hub = insert(:hub)
      assert %Ecto.Changeset{} = Hubs.change_hub(hub)
    end
  end
end
