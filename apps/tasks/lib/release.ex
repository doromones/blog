defmodule Tasks.Release do
  @repos [Blog.Repo]

  def migrations do
    IO.puts Enum.map_join(@repos, "\n", fn(repo)->
      repo_status = Ecto.Migrator.migrations(repo, migrations_path(repo))
      """
      Repo: #{inspect repo}
        Status    Migration ID    Migration Name
      --------------------------------------------------
      """ <>
      Enum.map_join(repo_status, "\n", fn({status, number, description}) ->
        status =
          case status do
            :up   -> "up  "
            :down -> "down"
          end

        "  #{status}      #{number}  #{description}"
      end) <> "\n"
    end)
  end

  def migrate do
    Enum.map(@repos, fn(repo)->
      IO.puts "Running migrations for #{inspect repo}"
      Ecto.Migrator.run(repo, migrations_path(repo), :up, all: true)
    end)
  end

  defp migrations_path(repo), do: priv_path_for(repo, "migrations")

  defp seeds_path(repo), do: priv_path_for(repo, "seeds.exs")

  defp priv_dir(app), do: "#{:code.priv_dir(app)}"

  defp priv_path_for(repo, filename) do
    app = Keyword.get(repo.config, :otp_app)
    repo_underscore = repo |> Module.split |> List.last |> Macro.underscore
    Path.join([priv_dir(app), repo_underscore, filename])
  end
end