defmodule BlogWeb.PostController do
  use BlogWeb, :controller
  alias Blog.Repo
  alias Blog.Posts
  alias Blog.Posts.Post

  plug :only_for_auth! when action in [:new, :create, :edit, :update, :delete]
  plug :must_be_owner! when action in [:edit, :update, :delete]

  def index(conn, _params, current_user) do
    posts = Posts.list_posts()
    render(conn, "index.json", posts: posts)
  end

  def new(conn, _params, current_user) do
    changeset = Posts.change_post(%Post{})
    render(conn, "new.html", changeset: changeset, hub: conn.assigns[:hub])
  end

  def create(conn, %{"post" => post_params}, current_user) do
    {ip, country_code} = geoip(conn)
    post_params = post_params
    |> Map.merge(%{
      "created_by_id" => current_user.id,
      "created_from_ip" => ip,
      "created_from_country_code" => country_code
    })

    case Posts.create_post(post_params) do
      {:ok, post} ->
        hub = Blog.Hubs.get_hub(post.hub_id)
        if hub do
          conn
          |> put_flash(:info, "Post created successfully.")
          |> redirect(external: to_subdomain(sub_post_url(conn, :show, post.slug), hub))
        else
          conn
          |> put_flash(:info, "Post created successfully.")
          |> redirect(to: page_path(conn, :index))
        end
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}, current_user) do
    post = Posts.get_post!(id) |> Blog.Repo.preload([:hub])
    render(conn, "show.html", post: post)
  end

  def edit(conn, _, current_user) do
    post = conn.assigns.post
    changeset = Posts.change_post(post)
    render(conn, "edit.html", post: post, changeset: changeset)
  end

  def update(conn, %{"post" => post_params}, current_user) do
    {ip, country_code} = geoip(conn)
    post_params = Map.merge(
      post_params,
      %{
        "updated_by_id" => current_user.id,
        "updated_from_ip" => ip,
        "updated_from_country_code" => country_code
      }
    )
    post = conn.assigns.post
    case Posts.update_post(post, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: sub_post_path(conn, :show, post.slug))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", post: post, changeset: changeset)
    end
  end

  def delete(conn, _, current_user) do
    post = conn.assigns.post |> Repo.preload(:hub)
    {:ok, _post} = Posts.delete_post(post)

    conn
    |> put_flash(:info, "Post deleted successfully.")
    |> redirect(external: to_subdomain(sub_hub_url(conn, :show), post.hub))
  end

  def must_be_owner!(%Plug.Conn{params: %{"id" => id}} = conn, _) do
    post = Posts.get_post!(id) |> Blog.Repo.preload([:hub])
    if post.created_by_id == current_user(conn).id do
      conn
      |> assign(:post, post)
    else
      conn
      |> redirect(external: to_subdomain(sub_hub_url(conn, :show), conn.assigns.hub))
      |> halt
    end
  end
end
