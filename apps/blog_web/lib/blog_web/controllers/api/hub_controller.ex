defmodule BlogWeb.Api.HubController do
  use BlogWeb, :controller

  alias Blog.Hubs
  alias Blog.Hubs.Hub

  action_fallback BlogWeb.FallbackController

  def index(conn, params, _) do
    hubs = Hubs.list_hubs(params["name"])
    render(conn, "index.json", hubs: hubs)
  end

#  def create(conn, %{"hub" => hub_params}) do
#    with {:ok, %Hub{} = hub} <- Hubs.create_hub(hub_params) do
#      conn
#      |> put_status(:created)
#      |> put_resp_header("location", api_hub_path(conn, :show, hub))
#      |> render("show.json", hub: hub)
#    end
#  end
#
#  def show(conn, %{"id" => id}) do
#    hub = Hubs.get_hub!(id)
#    render(conn, "show.json", hub: hub)
#  end
#
#  def update(conn, %{"id" => id, "hub" => hub_params}) do
#    hub = Hubs.get_hub!(id)
#
#    with {:ok, %Hub{} = hub} <- Hubs.update_hub(hub, hub_params) do
#      render(conn, "show.json", hub: hub)
#    end
#  end
#
#  def delete(conn, %{"id" => id}) do
#    hub = Hubs.get_hub!(id)
#    with {:ok, %Hub{}} <- Hubs.delete_hub(hub) do
#      send_resp(conn, :no_content, "")
#    end
#  end
end
