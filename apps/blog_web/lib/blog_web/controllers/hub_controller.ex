defmodule BlogWeb.HubController do
  use BlogWeb, :controller

  alias Blog.Hubs
  alias Blog.Hubs.Hub
  alias Blog.Repo

  plug :only_for_auth! when action in [:new, :create, :edit, :update, :delete]
  plug :must_be_owner! when action in [:edit, :update, :delete]

  def index(conn, _params, _) do
    hubs = Hubs.list_hubs()
    render(conn, "index.html", hubs: hubs)
  end

  def new(conn, _params, _) do
    changeset = Hubs.change_hub(%Hub{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"hub" => hub_params}, current_user) do
    {ip, country_code} = geoip(conn)
    hub_params = Map.merge(
      hub_params,
      %{
        "created_by_id" => current_user.id,
        "created_from_ip" => ip,
        "created_from_country_code" => country_code
      }
    )
    case Hubs.create_hub(hub_params) do
      {:ok, hub} ->
        conn
        |> put_flash(:info, "Hub created successfully.")
        |> redirect(external: to_subdomain(sub_hub_url(conn, :show), hub))
      {:error, %Ecto.Changeset{} = changeset} ->
        Apex.ap changeset
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(%Plug.Conn{assigns: %{hub: hub}} = conn, _, current_user) do
    posts = Blog.Posts.list_posts_by_hub(hub)
    render(conn, "show.html", hub: hub, posts: posts, current_user: current_user)
  end

  def edit(%Plug.Conn{assigns: %{hub: hub}} = conn, _, _) do
    changeset = Hubs.change_hub(hub)
    render(conn, "edit.html", hub: hub, changeset: changeset)
  end

  def update(%Plug.Conn{assigns: %{hub: hub}} = conn, %{"hub" => hub_params}, current_user) do
    {ip, country_code} = geoip(conn)
    hub_params = Map.merge(
      hub_params,
      %{
        "updated_by_id" => current_user.id,
        "updated_from_ip" => ip,
        "updated_from_country_code" => country_code
      }
    )
    case Hubs.update_hub(hub, hub_params) do
      {:ok, hub} ->
        conn
        |> put_flash(:info, "Hub updated successfully.")
        |> redirect(external: to_subdomain(sub_hub_url(conn, :show), hub))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", hub: hub, changeset: changeset)
    end
  end

  def delete(%Plug.Conn{assigns: %{hub: hub}} = conn, _, _) do
    {:ok, _hub} = Hubs.delete_hub(hub)

    conn
    |> put_flash(:info, "Hub deleted successfully.")
    |> redirect(external: page_url(conn, :index))
  end

  def must_be_owner!(conn, _) do
    if conn.assigns.hub.created_by_id == current_user(conn).id do
      conn
    else
      conn
      |> redirect(to: to_subdomain(sub_hub_url(conn, :show), conn.assigns.hub))
      |> halt
    end
  end
end
