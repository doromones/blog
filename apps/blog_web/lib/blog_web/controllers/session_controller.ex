defmodule BlogWeb.SessionController do
  use BlogWeb, :controller
  alias Blog.Account
  alias Blog.Account.User

  plug :only_for_nonauth! when action in [:new, :create]
  plug :only_for_auth! when action in [:delete]

  def new(conn, _, _) do
    changeset = Account.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user}, _) do
    {ip, country_code} = geoip(conn)
    case Account.check_auth(user["username"], user["password"]) do
      {:ok, user} ->

        Account.update_user_after_auth(user, %{
          "last_login_from_ip" => ip,
          "last_login_from_country_code" => country_code,
          "last_login_at" => NaiveDateTime.utc_now
        })
        conn
        |> put_flash(:info, "Succefful logged")
        |> put_session(:current_user, user)
        |> redirect(to: "/")
      _ ->
        changeset = Account.user_changeset(%User{}, %{username: user["username"]})
        conn
        |> put_flash(:error, "User with password not found")
        |> render("new.html", changeset: changeset)
    end
  end

  def delete(conn, _, _) do
    conn
    |> clear_session
    |> put_flash(:info, "Succefful logout")
    |> redirect(to: page_path(conn, :index))
  end
end
