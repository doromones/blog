defmodule BlogWeb.PageController do
  use BlogWeb, :controller

  def index(conn, params, current_user) do
    posts = Blog.Posts.list_posts |> Blog.Repo.preload([:created_by, :hub])
    render(conn, "index.html", posts: posts, current_user: current_user)
  end
end
