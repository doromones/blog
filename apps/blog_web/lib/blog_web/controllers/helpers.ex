defmodule BlogWeb.Controllers.Helpers do
  import Plug.Conn
  import Phoenix.Controller

  def current_user(conn), do: Plug.Conn.get_session(conn, :current_user)

  def set_current_user(conn), do: conn |> assign(:current_user, current_user(conn))

  def logged?(conn), do: !!current_user(conn)

  def only_for_auth!(conn, _) do
    unless logged?(conn) do
      conn
      |> put_flash(:error, "Need auth")
      |> redirect(to: "/")
      |> halt
    else
      conn
    end
  end

  def only_for_nonauth!(conn, _) do
    if logged?(conn) do
      conn
      |> put_flash(:info, "You already auth")
      |> redirect(to: "/")
      |> halt
    else
      conn
    end
  end

  def to_subdomain(url, %Blog.Hubs.Hub{url: subdomain} = hub) do
    to_subdomain(url, subdomain)
  end

  def to_subdomain(url, subdomain) when is_binary(subdomain) do
    uri = URI.parse(url)
    uri = Map.merge(uri, %{host: "#{subdomain}.#{uri.host}"})
    URI.to_string(uri)
  end

  def geoip(%{remote_ip: remote_ip} = conn) do
    case Geolix.lookup(remote_ip) do
      %{
        country: %{
          country: %{
            iso_code: iso_code
          }
        }
      } -> {remote_ip, iso_code}
      _ -> {remote_ip, nil}
    end
  end
end