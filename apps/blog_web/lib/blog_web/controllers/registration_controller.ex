defmodule BlogWeb.RegistrationController do
  use BlogWeb, :controller
  alias Blog.Account
  alias Blog.Account.User

  plug :only_for_nonauth!

  def new(conn, _, _) do
    changeset = Account.change_user(%User{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user}, _) do
    {ip, country_code} = geoip(conn)
    user = Map.merge(user, %{"registered_from_ip" => ip, "registered_from_country_code" => country_code})
    case Account.create_user(user) do
      {:ok, user} ->
        conn
        |> put_session(:current_user, user)
        |> put_flash(:info, "Registration created successfully.")
        |> redirect(to: "/")
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end
end
