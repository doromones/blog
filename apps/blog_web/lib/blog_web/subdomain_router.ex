defmodule BlogWeb.SubdomainRouter do
  use BlogWeb, :router

  pipeline :browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  scope "/", BlogWeb, as: :sub do
    pipe_through :browser # Use the default browser stack

#    resources "/hubs", HubController, only: [:new, :create]
    get "/", HubController, :show
    get "/edit", HubController, :edit
    put "/", HubController, :update
    delete "/", HubController, :delete

    resources "/session", SessionController, only: [:new, :create, :delete]
    resources "/registration", RegistrationController, only: [:new, :create]

    resources "/posts", PostController, only: [:show, :new, :create, :edit, :update, :delete]
  end

  # Other scopes may use custom stacks.
#  scope "/api", BlogWeb.Api do
#    pipe_through :api
#    resources "/hubs", HubController, only: [:index]
#  end
end