defmodule BlogWeb.Plug.Subdomain do
  import Plug.Conn
  alias Blog.Hubs

  @doc false
  def init(default), do: default

  @doc false
  def call(conn, routers) do
    [subdomain_router, default_router] = routers
    case get_subdomain(conn.host) do
      subdomain when byte_size(subdomain) > 0 ->
        hub = Hubs.get_hub!(subdomain)
        conn
        |> assign(:hub, hub)
        |> subdomain_router.call(subdomain_router.init({}))
      _ ->
        conn
        |> default_router.call(default_router.init({}))
    end
  end

  defp get_subdomain(host) do
    root_host = BlogWeb.Endpoint.config(:url)[:host]
    hub_name = String.replace(host, ~r/.?#{root_host}/, "")
    hub_name
  end
end