defmodule BlogWeb.Router do
  use BlogWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
  end

  scope "/", BlogWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/session", SessionController, only: [:new, :create, :delete]
    resources "/registration", RegistrationController, only: [:new, :create]
    resources "/hubs", HubController, only: [:new, :index, :create]
    resources "/posts", PostController, only: [:new, :create]
  end

  # Other scopes may use custom stacks.
  scope "/api", BlogWeb.Api do
    pipe_through :api
    resources "/hubs", HubController, only: [:index]
    resources "/comments", CommentController, except: [:new, :edit]
  end

  scope "/admin" do
    pipe_through :browser
    forward "/", AdminWeb.Endpoint, namespace: "admin"
  end
end
