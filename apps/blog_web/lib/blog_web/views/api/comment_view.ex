defmodule BlogWeb.CommentView do
  use BlogWeb, :view
  alias BlogWeb.CommentView

  def render("index.json", %{comments: comments}) do
    %{data: render_many(comments, CommentView, "comment.json")}
  end

  def render("show.json", %{comment: comment}) do
    %{data: render_one(comment, CommentView, "comment.json")}
  end

  def render("comment.json", %{comment: comment}) do
    %{id: comment.id,
      created_from_ip: comment.created_from_ip,
      created_from_country_code: comment.created_from_country_code,
      updated_from_ip: comment.updated_from_ip,
      updated_from_country_code: comment.updated_from_country_code,
      body: comment.body}
  end
end
