defmodule BlogWeb.Api.HubView do
  use BlogWeb, :view
  alias BlogWeb.Api.HubView

  def render("index.json", %{hubs: hubs}) do
    %{hubs: render_many(hubs, HubView, "hub.json")}
  end

#  def render("show.json", %{hub: hub}) do
#    %{data: render_one(hub, HubView, "hub.json")}
#  end

  def render("hub.json", %{hub: hub}) do
    %{
      id: hub.id,
      url: hub.url,
      name: hub.name
    }
  end
end
