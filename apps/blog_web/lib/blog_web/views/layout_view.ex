defmodule BlogWeb.LayoutView do
  use BlogWeb, :view

  def is_preloaded(model, assoc) do
    case Map.get(model, assoc) do
      %Ecto.Association.NotLoaded{} -> false
      _ -> true
    end
  end

  def date_to_string(date) do
    date_now = NaiveDateTime.utc_now |> Calendar.Strftime.strftime!("%Y-%m-%d")
    date_tommorow = NaiveDateTime.utc_now |> NaiveDateTime.add(-1 * 60*60*24) |> Calendar.Strftime.strftime!("%Y-%m-%d")

    cond do
      Calendar.Strftime.strftime!(date, "%Y-%m-%d") == date_now ->
        "Сегодня #{Calendar.Strftime.strftime!(date, "%H:%M")}"
      Calendar.Strftime.strftime!(date, "%Y-%m-%d") == date_tommorow ->
        "Вчера #{Calendar.Strftime.strftime!(date, "%H:%M")}"
      true ->
        Calendar.Strftime.strftime!(date, "%Y-%m-%d %H:%M")
    end
  end

  def sanitize(html) do
    html |> HtmlSanitizeEx.Scrubber.scrub(BlogWeb.Sanitizers.BasicHTML)
  end
end
