defmodule BlogWeb.HubView do
  use BlogWeb, :view

  def render("index.json", %{hubs: hubs}) do
    %{
      hubs: Enum.map(hubs, &hub_json/1)
    }
  end

  def hub_json(hub) do
    %{
      id: hub.id,
      url: hub.url,
      name: hub.name
    }
  end
end
