defmodule BlogWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use BlogWeb, :controller
      use BlogWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: BlogWeb
      import Plug.Conn
      import BlogWeb.Router.Helpers
      import BlogWeb.SubdomainRouter.Helpers, except: [static_path: 2]
      import BlogWeb.Gettext
      import BlogWeb.Controllers.Helpers

      def action(%{params: params} = conn, _opts) do
        conn = conn |> set_current_user
        apply(__MODULE__, action_name(conn), [conn, params, conn.assigns.current_user])
      end
    end
  end

  def view do
    quote do
      use Phoenix.View, root: "lib/blog_web/templates",
                        namespace: BlogWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 2, view_module: 1]

      # Use all HTML functionality (forms, tags, etc)
      use Phoenix.HTML

      import BlogWeb.Router.Helpers
      import BlogWeb.SubdomainRouter.Helpers, except: [static_path: 2]
      import BlogWeb.Controllers.Helpers, only: [current_user: 1, logged?: 1, to_subdomain: 2]
      import BlogWeb.ErrorHelpers
      import BlogWeb.Gettext
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import BlogWeb.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
