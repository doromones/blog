defmodule BlogWeb.TestHelpers do
  import Plug.Test

  def login(conn, %Blog.Account.User{} = user) do
    conn |> init_test_session(current_user: user)
  end

  def rand_string do
    :crypto.strong_rand_bytes(5)
    |> Base.encode64
    |> binary_part(0, 5)
  end
end

