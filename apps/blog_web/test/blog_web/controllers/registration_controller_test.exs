defmodule BlogWeb.RegistrationControllerTest do
  use BlogWeb.ConnCase

  @create_attrs %{username: "username", email: "email@example.com", password: "password", password_confirmation: "password"}
  @invalid_attrs %{}

  describe "new registration" do
    test "renders form", %{conn: conn} do
      conn = get conn, registration_path(conn, :new)
      assert html_response(conn, 200) =~ "New Registration"
    end
  end

  describe "create registration" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, registration_path(conn, :create), user: @create_attrs
      assert redirected_to(conn) == page_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, registration_path(conn, :create), user: @invalid_attrs
      assert html_response(conn, 200) =~ "New Registration"
    end
  end
end
