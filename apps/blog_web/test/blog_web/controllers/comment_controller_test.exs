defmodule BlogWeb.CommentControllerTest do
  use BlogWeb.ConnCase

  alias Blog.Comments
  alias Blog.Comments.Comment

  @create_attrs %{body: "some body", created_from_country_code: "some created_from_country_code", created_from_ip: "some created_from_ip", updated_from_country_code: "some updated_from_country_code", updated_from_ip: "some updated_from_ip"}
  @update_attrs %{body: "some updated body", created_from_country_code: "some updated created_from_country_code", created_from_ip: "some updated created_from_ip", updated_from_country_code: "some updated updated_from_country_code", updated_from_ip: "some updated updated_from_ip"}
  @invalid_attrs %{body: nil, created_from_country_code: nil, created_from_ip: nil, updated_from_country_code: nil, updated_from_ip: nil}

  def fixture(:comment) do
    {:ok, comment} = Comments.create_comment(@create_attrs)
    comment
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all comments", %{conn: conn} do
      conn = get conn, comment_path(conn, :index)
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create comment" do
    test "renders comment when data is valid", %{conn: conn} do
      conn = post conn, comment_path(conn, :create), comment: @create_attrs
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get conn, comment_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "body" => "some body",
        "created_from_country_code" => "some created_from_country_code",
        "created_from_ip" => "some created_from_ip",
        "updated_from_country_code" => "some updated_from_country_code",
        "updated_from_ip" => "some updated_from_ip"}
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, comment_path(conn, :create), comment: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update comment" do
    setup [:create_comment]

    test "renders comment when data is valid", %{conn: conn, comment: %Comment{id: id} = comment} do
      conn = put conn, comment_path(conn, :update, comment), comment: @update_attrs
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get conn, comment_path(conn, :show, id)
      assert json_response(conn, 200)["data"] == %{
        "id" => id,
        "body" => "some updated body",
        "created_from_country_code" => "some updated created_from_country_code",
        "created_from_ip" => "some updated created_from_ip",
        "updated_from_country_code" => "some updated updated_from_country_code",
        "updated_from_ip" => "some updated updated_from_ip"}
    end

    test "renders errors when data is invalid", %{conn: conn, comment: comment} do
      conn = put conn, comment_path(conn, :update, comment), comment: @invalid_attrs
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete comment" do
    setup [:create_comment]

    test "deletes chosen comment", %{conn: conn, comment: comment} do
      conn = delete conn, comment_path(conn, :delete, comment)
      assert response(conn, 204)
      assert_error_sent 404, fn ->
        get conn, comment_path(conn, :show, comment)
      end
    end
  end

  defp create_comment(_) do
    comment = fixture(:comment)
    {:ok, comment: comment}
  end
end
