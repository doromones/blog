defmodule BlogWeb.PostControllerTest do
  use BlogWeb.ConnCase
  import Blog.Factory
  alias Blog.Posts
  alias Blog.Repo

  @update_attrs %{body: "some updated body", cut: "some updated cut", title: "some updated title", url: "http://example.com"}
  @invalid_attrs %{body: "", cut: "", title: "", url: ""}

  describe "new post" do
    test "renders form", %{conn: conn} do
      conn = conn
             |> login(insert(:user))
             |> get(to_subdomain(sub_post_url(conn, :new), insert(:hub)))
      assert html_response(conn, 200) =~ "New Post"
    end
  end

  describe "create post" do
    test "redirects to show when data is valid", %{conn: conn} do
      post_params = string_params_for(:post)
      hub = insert(:hub)
      conn = conn
             |> login(insert(:user))
             |> post(to_subdomain(sub_post_url(conn, :create), hub), post: post_params)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == sub_post_path(conn, :show, id)

      conn = get conn, to_subdomain(sub_post_url(conn, :show, id), hub)
      assert html_response(conn, 200) =~ "Show Post"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = conn
             |> login(insert(:user))
             |> post(to_subdomain(sub_post_url(conn, :create), insert(:hub)), post: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Post"
    end
  end

  describe "edit post" do
    setup [:create_post]

    test "renders form for editing chosen post", %{conn: conn, post: post} do
      conn = conn
             |> login(post.created_by)
             |> get(to_subdomain(sub_post_url(conn, :edit, post.slug), post.hub))
      assert html_response(conn, 200) =~ "Edit Post"
    end
  end

  describe "update post" do
    setup [:create_post]

    test "redirects when data is valid", %{conn: conn, post: post} do
      conn = conn
             |> login(post.created_by)
             |> put(to_subdomain(sub_post_url(conn, :update, post.slug), post.hub), post: @update_attrs)
      slug = Slug.slugify("#{@update_attrs.title} #{post.id}")
      assert redirected_to(conn) == sub_post_path(conn, :show, slug)

      conn = get conn, to_subdomain(sub_post_url(conn, :show, slug), post.hub)
      assert html_response(conn, 200) =~ "some updated body"
    end

    test "renders errors when data is invalid", %{conn: conn, post: post} do
      conn = conn
             |> login(post.created_by)
             |> put(to_subdomain(sub_post_url(conn, :update, post.slug), post.hub), post: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Post"
    end
  end

  describe "delete post" do
    setup [:create_post]

    test "deletes chosen post", %{conn: conn, post: post} do
      conn = conn
             |> login(post.created_by)
             |> delete(to_subdomain(sub_post_url(conn, :delete, post.slug), post.hub))
      assert redirected_to(conn) == to_subdomain(sub_hub_url(conn, :show), post.hub)
      assert_error_sent 404, fn ->
        get conn, sub_post_path(conn, :show, post)
      end
    end
  end

  defp create_post(_) do
    {:ok, post} = Posts.create_post(string_params_for(:post, hub_id: insert(:hub).id))
    post = post |> Repo.preload([:created_by, :hub])
    {:ok, post: post}
  end
end
