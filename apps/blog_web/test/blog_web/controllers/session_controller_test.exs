defmodule BlogWeb.SessionControllerTest do
  use BlogWeb.ConnCase
  import Blog.Factory

  @invalid_attrs %{username: "", password: ""}

  setup %{conn: conn} do
    %{conn: conn, user: insert(:user)}
  end

  describe "new session" do
    test "renders form", %{conn: conn} do
      conn = get conn, session_path(conn, :new)
      assert html_response(conn, 200) =~ "New Session"
    end
  end

  describe "create session" do
    test "redirects to show when data is valid", %{conn: conn, user: user} do
      conn = post conn, session_path(conn, :create), user: %{username: user.username, password: "password"}
      assert redirected_to(conn) == page_path(conn, :index)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, session_path(conn, :create), user: @invalid_attrs
      assert get_flash(conn, :error) == "User with password not found"
      assert html_response(conn, 200) =~ "New Session"
    end
  end

  describe "delete session" do
    test "deletes chosen session", %{conn: conn, user: user} do
      conn = conn
      |> login(user)
      |> delete(session_path(conn, :delete, "me"))
      assert get_flash(conn, :info) == "Succefful logout"
      assert redirected_to(conn) == page_path(conn, :index)
    end
  end
end
