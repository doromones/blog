defmodule BlogWeb.HubControllerTest do
  use BlogWeb.ConnCase
  import Blog.Factory

  @create_attrs %{adult: true, description: "some description", name: "some name", url: "url"}
  @update_attrs %{adult: false, description: "some updated description", name: "some updated name"}
  @invalid_attrs %{adult: nil, description: nil, name: nil}

#  describe "index" do
#    test "lists all hubs", %{conn: conn} do
#      conn = get conn, hub_path(conn, :index)
#      assert html_response(conn, 200) =~ "Listing Hubs"
#    end
#  end

  describe "new hub" do
    test "renders form", %{conn: conn} do
      conn = conn
      |> login(insert(:user))
      |> get(hub_path(conn, :new))
      assert html_response(conn, 200) =~ "New Hub"
    end
  end

  describe "create hub" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = conn
             |> login(insert(:user))
             |> post(hub_path(conn, :create), hub: @create_attrs)
      assert redirected_to(conn) == to_subdomain(sub_hub_url(conn, :show), @create_attrs.url)

      conn = get conn, to_subdomain(sub_hub_url(conn, :show), @create_attrs.url)
      assert html_response(conn, 200) =~ "Show Hub"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = conn
      |> login(insert(:user))
      |> post(hub_path(conn, :create), hub: @invalid_attrs)

      assert html_response(conn, 200) =~ "New Hub"
    end
  end

  describe "edit hub" do
    setup [:create_hub]

    test "renders form for editing chosen hub", %{conn: conn, hub: hub} do
      conn  = conn
      |> login(hub.created_by)
      |> get(to_subdomain(sub_hub_url(conn, :edit), hub))
      assert html_response(conn, 200) =~ "Edit Hub"
    end
  end

  describe "update hub" do
    setup [:create_hub]

    test "redirects when data is valid", %{conn: conn, hub: hub} do
      conn = conn
      |> login(hub.created_by)
      |> put(to_subdomain(sub_hub_url(conn, :update), hub), hub: @update_attrs)

      assert redirected_to(conn) == to_subdomain(sub_hub_url(conn, :show), hub)

      conn = get conn, to_subdomain(sub_hub_url(conn, :show), hub)
      assert html_response(conn, 200) =~ "some updated description"
    end

    test "renders errors when data is invalid", %{conn: conn, hub: hub} do
      conn = conn
      |> login(hub.created_by)
      |> put(to_subdomain(sub_hub_url(conn, :update), hub), hub: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Hub"
    end
  end

  describe "delete hub" do
    setup [:create_hub]

    test "deletes chosen hub", %{conn: conn, hub: hub} do
      conn = conn
      |> login(hub.created_by)
      |> delete(to_subdomain(sub_hub_url(conn, :delete), hub))

      assert redirected_to(conn) == page_url(conn, :index)
      assert_error_sent 404, fn ->
        get conn, to_subdomain(sub_hub_url(conn, :show), hub)
      end
    end
  end

  defp create_hub(_) do
    hub = insert(:hub, @create_attrs)
    {:ok, hub: hub}
  end
end
