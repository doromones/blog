window['summernote_config'] = {
  toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['link', ['linkDialogShow', 'unlink']],
    ['image', []],
    ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'help']]
  ]
};