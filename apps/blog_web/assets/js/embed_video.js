const embed = require("embed-video");

const embed_links_masks = [
  'a[href^="https://youtu.be/"]',
  'a[href^="https://vimeo.com/"]'
];

$('.post-body').find(embed_links_masks.join(',')).each(function(_, link){
  let wrapper = $("<div>").addClass("embed-responsive embed-responsive-16by9");

  embed.image(link.href, null, function(err, thumb){
    let preview = $(thumb.html).addClass("embed-responsive-item").on('click', function(){
      $(this).replaceWith(embed(link.href));
    });
    wrapper.append(preview);
  });
  console.log(wrapper);
  $(link).replaceWith(wrapper);
});
