$(()=>{
  const hubList = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: '/api/hubs?name=%QUERY',
      wildcard: '%QUERY',
      transform: (response) => response.hubs
    }
  });

  $('#post_hub_name').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  }, {
    name: 'hubs',
    display: 'name',
    source: hubList
  }).bind('typeahead:select', function(ev, hub) {
    $('#post_hub_id').val(hub.id);
  });

  $('#post_cut').summernote(window.summernote_config);
  $('#post_body').summernote(window.summernote_config);
});