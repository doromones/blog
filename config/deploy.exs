use Bootleg.Config

config(:app, :blog_umbrella)
config(:apps, ["admin_web", "blog", "blog_web"])
config(:db_apps, ["blog"])
config(:web_apps, ["admin_web", "blog_web"])
config(:version, "0.0.1")
config(:build_path, "/tmp/bootleg/build/#{config(:app)}")
config(:app_path, "/var/www/#{config(:app)}")
config(:config_secret_path, "/var/www/blog")
config(:refspec, "deploy_bootleg") # current branch

role :build,
     "195.201.102.125",
     workspace: config(:build_path),
     user: "user",
     silently_accept_hosts: true

task :copy_secret_file do
  alias Bootleg.UI
  config(:apps) |> Enum.each(fn(app)->
    UI.info "Create symlink for #{app}/config/prod.secret.exs"
    remote :build do
      "cp #{config(:config_secret_path)}/#{app}_prod.secret.exs #{config(:build_path)}/apps/#{app}/config/prod.secret.exs"
    end
  end)
end

task :phoenix_digest do
  alias Bootleg.UI

  config(:web_apps) |> Enum.each(fn(app)->
    dir = "apps/#{app}"
    assets = "#{dir}/assets"
    env = Keyword.get(config(), :mix_env, "prod")
    remote :build do
      "[ ! -d #{dir}/priv/static ] && mkdir -p #{dir}/priv/static"
      "[ -f #{assets}/package.json ] && cd #{assets} && npm install || true"
      "[ -f #{assets}/brunch-config.js ] && [ -d #{assets}/node_modules ] && cd #{assets} && ./node_modules/brunch/bin/brunch b -p || true"
      "[ -d #{config(:build_path)}/deps/phoenix ] && MIX_ENV=#{env} mix phoenix.digest || true"
    end
    UI.info "Phoenix asset for #{app} digest generated"
  end)
  UI.info "All phoenix asset digest generated"
end

task :migrations do
  alias Bootleg.{UI, Config}
  UI.info "Show migrations"
  [{:ok, [stdout: output], _, _}] = remote :app do
    "bin/#{Config.app} rpc Elixir.Tasks.Release migrations"
  end
  IO.puts output
end

task :migrate do
  alias Bootleg.{UI, Config}
  UI.info "Run migrations"
  [{:ok, output,_,_}] = remote :app do
    "bin/#{Config.app} rpc Elixir.Tasks.Release migrate"
  end
  output = Enum.map_join(output, "\n", fn({:stdout, str})-> str  end)
  IO.puts output
end

before_task(:compile, :copy_secret_file)
after_task(:compile, :phoenix_digest)
after_task(:update, :migrate)
